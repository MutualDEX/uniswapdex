require("dotenv").config();
const fs = require("fs");
const http = require("http");
const https = require("https");
const express = require("express");
const cors = require("cors");
const compression = require("compression");
const CONFIG = require("./auth-server-config");
const utils = require("./utils");
const path = require("path");
let cons = require("consolidate");
const requestIp = require("request-ip");
const resourcePath = path.resolve(CONFIG.staticDirectory);
const crypto = require("shardus-crypto-utils");
crypto.init("69fa4195670576c0160d660c3be36556ff8d504725be8a59b5a96509e0c994bc");
const app = express();
app.use(compression());

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Origin", req.headers.origin);
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.header(
    "Access-Control-Allow-Headers",
    "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept"
  );
  if (req.method === "OPTIONS") {
    res.send(200);
  } else {
    next();
  }
});

app.use(cors());
app.use((req, res, next) => {
  if (CONFIG.environment === "production" && !req.secure) {
    res.redirect("https://" + req.headers.host + req.url);
    return;
  }
  next();
});

app.use("/_nuxt", express.static(resourcePath));
app.engine("html", cons.swig);
app.set("view engine", "html");
app.set("views", CONFIG.viewDirectory);

app.get(CONFIG.mainRoute, (req, res) => {
  res.render("index.html");
});

app.get("/sitekey", async (req, res) => {
  const recaptchaToken = req.query.recaptchatoken;
  const IP = requestIp.getClientIp(req);
  const isRecaptchaTokenValid = await utils.checkRecaptcha(recaptchaToken, IP);
  if (isRecaptchaTokenValid) {
    const S = process.env.UNISWAPDEX_CLIENT_SECRET;
    const I = crypto.hashObj({ data: [IP, S] });
    return res.send(I);
  } else {
    console.log("Invalid reCAPTCHA code...");
    return res.send(crypto.hashObj({ data: null }));
  }
});
const httpServer = http.createServer(app);
httpServer.listen(CONFIG.port.http, err => {
  if (err) console.log(err);
  console.log(`HTTP server is listening at port ${CONFIG.port.http}`);
});
if (CONFIG.environment === "production") {
  const privateKey = fs.readFileSync(
    `${CONFIG.keyLocation}/privkey.pem`,
    "utf8"
  );
  const certificate = fs.readFileSync(`${CONFIG.keyLocation}/cert.pem`, "utf8");
  const ca = fs.readFileSync(`${CONFIG.keyLocation}/chain.pem`, "utf8");
  const credentials = {
    key: privateKey,
    cert: certificate,
    ca: ca
  };
  const httpsServer = https.createServer(credentials, app);
  httpsServer.listen(CONFIG.port.https, err => {
    if (err) console.log(err);
    console.log(`HTTPS server is listening at port ${CONFIG.port.https}`);
  });
}
